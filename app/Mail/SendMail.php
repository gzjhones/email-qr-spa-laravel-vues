<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    //Public Variables 
    public $subject;
    //public $email;
    public $text;
    public $qr;
    
    //Array $data received from EmailController->sendEmail
    public function __construct($data)
    {
        $this->subject  = $data['subject'];
        //$this->email    = $data['email'];
        $this->text     = $data['text'];
        $this->qr       = $data['qr'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    //Sent to the view in blade.php format in /views/emails/sendEmail.blade.php
    //with all public variables
    
    public function build()
    {
        return $this->view('emails.sendEmail');
    }
}
