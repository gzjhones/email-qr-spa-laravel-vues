<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Import SendaMail and Mail
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

//Import QR Class
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class EmailController extends Controller
{
    public function sendEmail(Request $request){
        
        //Generate QR
        $qr = QrCode::format('png')->size(200)->generate($request['text']);

        $data = array(
            'subject'   => $request['subject'],
            'email'     => $request['email'],
            'text'      => $request['text'],
            'qr'        => $qr,
        );

        Mail::to($data['email'])->send(new SendMail($data));

        return response()->json(true);
    }
}
