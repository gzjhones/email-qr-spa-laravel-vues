# Send QR in Email SPA Laravel VueJS.

Sending QR through email through gmail services, in SPA Laravel VueJs application

## Getting Started
Make sure you have `npm` and `composer` on your local machine

### Steps 5 - 7 included in the repository, must be used in projects where you want to implement

1.  Clone this repository and cd into it

2.  Run `composer install` and `npm install` to download laravel dependencies php and node

3.  Run `php artisan key:generate` to generate a key for the app

4.  Set up your `.env` file according to `.env.example`

    MAIL_DRIVER=smtp<br>
    MAIL_HOST=smtp.gmail.com<br>
    MAIL_PORT=587<br>
    MAIL_FROM_NAME="Alias of Email"<br>
    MAIL_FROM_ADDRESS=youemail@gmail.com<br>
    MAIL_USERNAME=youemail@gmail.com<br>
    MAIL_PASSWORD=xxxyyyzzzxxxyyzz<br>
    MAIL_ENCRYPTION=tls<br>
    
    ### MAIL_PASSWORD: This is an authorization code to send emails through third-party applications, it is not your Gmail password

    ### MAIL_PASSWORD: Este es un código de autorización para enviar emails mediante aplicaciones de terceros, no es su password Gmail

    Official documentation
    https://support.google.com/accounts/answer/185833?hl=en

    Third-party tutorial
    https://devanswers.co/create-application-specific-password-gmail/

5.  Install `Simple QR Composer`:

    `composer require simplesoftwareio/simple-qrcode`

6.  Make Class SendEmail:
    
    `php artisan make:mail SendMail`

7.  Make `/views/email/sendEmail.blade.php`

8.  Run `php artisan migrate` (In this example no database is used, but for good practices we link the database)

9. Run `composer dumpautoload`

10. Execute `npm run dev` to build project assets

11. Execute `php artisan serve` to run the project in your browser


### Prerequisites

* Laravel
* Vue
* Vuex
* npm


## Built With

* [Laravel](https://laravel.com/) - Beautiful Php framework
* [Vue](https://vuejs.org/) - A Great reactive Js framework
* [Vuex](https://vuejs.org/) - Vuejs state management made simple
* [Bootstrap](https://getbootstrap.com) - A beautiful Css framework
* [Axios](https://vuejs.org/) - A Js library to handle ajax requests easily

